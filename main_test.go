package main

import (
	"fmt"
	"math/rand"
	"testing"
)

func BenchmarkPlay(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = Play(1, false)
	}
}

func TestIntnAverage(t *testing.T) {
	const Turns = 1e5
	sum := 0
	for i := 0; i < Turns; i++ {
		n := rand.Intn(2) // range [0, n[
		sum += n
	}
	fmt.Printf("Average: %f\n", float64(sum)/Turns)
}

func TestPlay(t *testing.T) {
	_ = Play(1, true) // verbose for coverage purposes
}
