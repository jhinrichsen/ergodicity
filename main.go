package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"
)

func main() {
	games := flag.Int("games", 100000, "number of games to play")
	seed := flag.Bool("seed", true, "seed random number generator using "+
		"current time")
	verbose := flag.Bool("verbose", false, "verbose output")
	flag.Parse()

	if *seed {
		rand.Seed(time.Now().UnixNano())
	}

	r := Play(*games, *verbose)
	fmt.Printf("%.1f%% winners after %d games\n", 100*r, *games)
}

// Play n games and return winner ratio.
func Play(games int, verbose bool) float64 {
	const (
		InitialPoints = 100
		Players       = 10
		Turns         = 100
	)

	totalWinners := 0
	for game := 0; game < games; game++ {
		var points [Players]float64
		for player := 0; player < Players; player++ {
			points[player] = InitialPoints
		}
		if verbose {
			fmt.Printf("about to begin %d turns\n", Turns)
		}
		for turn := 0; turn < Turns; turn++ {
			for player := 0; player < Players; player++ {
				win := Throw()
				if win {
					points[player] *= 1.5
				} else {
					points[player] *= 0.6
				}
			}
		}

		winners := 0
		for player := 0; player < Players; player++ {
			winner := points[player] >= InitialPoints
			if winner {
				winners++
				if verbose {
					fmt.Printf("player #%d %s (%v points)\n",
						player, "wins", points[player])
				}
			} else {
				if verbose {
					fmt.Printf("player #%d %s (%v points)\n",
						player, "loses", points[player])
				}
			}
		}
		if verbose {
			fmt.Printf("winners: %d, losers: %d\n",
				winners, Players-winners)
		}
		totalWinners += winners
	}
	return float64(totalWinners) / float64(games) / Players
}

// Throw a perfect coin, return true if first side is on top, false if second.
func Throw() bool {
	return rand.Intn(2) == 0
}
